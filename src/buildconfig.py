# Copyright (c) 2014 Yang Li <bill.lee.y@gmail.com>
# All rights reserved.
import logging;

log_level = logging.ERROR;
log_level_gui = logging.CRITICAL;
log_format = '[%(levelname)s]%(name)s: %(message)s';
fdp_track_num = 4;
fdp_sector_num = 32;
