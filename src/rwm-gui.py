#!/usr/bin/env python
# Copyright (c) 2014 Yang Li <bill.lee.y@gmail.com>
# All rights reserved.

import buildconfig;
import sys;
import logging;
logger = logging.getLogger(__name__);

from PySide.QtCore import Qt;
from PySide.QtCore import Signal, Slot;
from PySide.QtCore import QThread;
from PySide.QtGui import QApplication, QMainWindow;
from PySide.QtGui import QDialog;
from PySide.QtGui import QImage, QPixmap;
from PySide.QtGui import QMessageBox;
from PySide.QtGui import QFileDialog;
from PySide.QtCore import QBuffer;
from cStringIO import StringIO;

from ui_rwm_gui import Ui_MainWindow;
from ui_about import Ui_About;

import PIL.Image;
from PIL.ImageQt import ImageQt as QImagePIL;
import rwm;
import derwm;

class AboutDialog(QDialog, Ui_About):
    def __init__(self, parent=None):
        super(AboutDialog, self).__init__(parent);
        self.setupUi(self);
        self.btnAboutQt.clicked.connect(QApplication.aboutQt);

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent);
        self.setupUi(self);
        self.imageDisplay.fileDropped.connect(self.loadImage);
        self.imageDisplay.clicked.connect(self.onImageDisplayClick);
        self.btnAttach.clicked.connect(self.onBtnAttachClicked);
        self.btnSave.clicked.connect(self.onBtnSaveClicked);
        self.btnDetect.clicked.connect(self.onBtnDetectClicked);
        self.btnAbout.clicked.connect(self.onBtnAboutClicked);

    @Slot(str)
    def loadImage(self, filename):
        logger.info("Loading image %s", filename);
        image = QImage();
        if image.load(filename):
            self.__image = image;
            self.onImageChanged();
            self.btnAttach.setEnabled(True);
            self.btnSave.setEnabled(False);
            self.btnDetect.setEnabled(True);
        else:
            QMessageBox.critical(self, 'Loading Image', 'Fail to load image %s' % filename);

    def saveImage(self, filename):
        logger.info("Saving image %s", filename);
        self.__image.save(filename);

    def onImageChanged(self):
        width = self.imageDisplay.width();
        height = self.imageDisplay.height();
        self.imageDisplay.setPixmap(\
                QPixmap.fromImage(self.__image)\
                .scaled(width, height, Qt.KeepAspectRatio, Qt.SmoothTransformation));

    @Slot()
    def onImageDisplayClick(self):
        filename = QFileDialog.getOpenFileName(self, 'Loading Image',\
                filter='Images (*.tif *.png *.jpg *.xpm *.bmp)');
        logger.debug("Filename from FileDialog: %s", str(filename));
        if filename[0] == '':
            return;
        self.loadImage(filename[0]);

    @Slot()
    def onBtnAttachClicked(self):
        self.btnAttach.setEnabled(False);
        self.btnDetect.setEnabled(False);
        self.attachingThread = AttachingThread(self.__image, bytes(self.attachingWatermarkText.text()), self);
        self.attachingThread.finished.connect(self.onAttachingFinished);
        self.attachingThread.start();

    @Slot()
    def onAttachingFinished(self):
        self.__image = QImagePIL(self.attachingThread.om);
        self.attachingThread = None;
        self.onImageChanged();
        self.btnSave.setEnabled(True);
        self.btnDetect.setEnabled(True);

    @Slot()
    def onBtnSaveClicked(self):
        self.btnSave.setEnabled(False);
        filename = QFileDialog.getSaveFileName(self, 'Saving Image',\
                filter='Portable Network Graphics (*.png);;JPEG Image (*.jpg);;Portable Bitmap (*.pbm)');
        if filename[0] == '':
            return;
        self.saveImage(filename[0]);

    @Slot()
    def onBtnDetectClicked(self):
        self.btnDetect.setEnabled(False);
        self.detectingWatermarkText.setText(None);
        self.detectingThread = DetectingThread(self.__image, self);
        self.detectingThread.finished.connect(self.onDetectingFinished);
        self.detectingThread.critical.connect(self.onWorkThreadCritical);
        self.detectingThread.start();

    @Slot()
    def onDetectingFinished(self):
        self.btnAttach.setEnabled(False);
        try:
            wm = self.detectingThread.text;
            self.detectingWatermarkText.setText(wm);
        except AttributeError as e:
            logger.debug(e);
            self.btnAttach.setEnabled(True);
        self.detectingThread = None;
        self.btnDetect.setEnabled(True);

    @Slot(str,str)
    def onWorkThreadCritical(self, title, message):
        QMessageBox.critical(self, title, message);

    @Slot()
    def onBtnAboutClicked(self):
        dialog = AboutDialog(self);
        dialog.show();

    def resizeEvent(self, event):
        try:
            self.__image;
        except AttributeError as e:
            logger.debug(e);
            return;
        width = self.imageDisplay.width();
        height = self.imageDisplay.height();
        self.imageDisplay.setPixmap(\
                QPixmap.fromImage(self.__image)\
                .scaled(width, height, Qt.KeepAspectRatio, Qt.SmoothTransformation));

class AttachingThread(QThread):
    def __init__(self, image, text, parent=None):
        super(AttachingThread, self).__init__(parent);
        self.__image = image;
        self.__text = text;

    def run(self):
        buffer = QBuffer();
        buffer.open(QBuffer.ReadWrite);
        self.__image.save(buffer, "PNG");
        strio = StringIO();
        strio.write(buffer.data());
        buffer.close();
        strio.seek(0);
        im = PIL.Image.open(strio);
        self.om = rwm.main_internal(im, self.__text);

class DetectingThread(QThread):
    critical = Signal(str,str);

    def __init__(self, image, parent=None):
        super(DetectingThread, self).__init__(parent);
        self.__image = image;

    def run(self):
        buffer = QBuffer();
        buffer.open(QBuffer.ReadWrite);
        self.__image.save(buffer, "PNG");
        strio = StringIO();
        strio.write(buffer.data());
        buffer.close();
        strio.seek(0);
        im = PIL.Image.open(strio);
        try:
            self.text = derwm.main_internal(im);
        except ValueError as e:
            self.critical.emit('Detecting', 'Fail to detect a watermark.\n%s' % str(e));

def main():
    logging.basicConfig(level=buildconfig.log_level_gui, format=buildconfig.log_format);
    logger.info('Robust Watermark');
    app = QApplication(sys.argv);
    mainwindow = MainWindow();
    mainwindow.show();
    return app.exec_();

if __name__=='__main__':
    sys.exit(main());
