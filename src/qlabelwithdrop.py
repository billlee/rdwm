# Copyright (c) 2014 Yang Li <bill.lee.y@gmail.com>
# All rights reserved.
import logging;
logger = logging.getLogger(__name__);

from PySide.QtCore import Qt;
from PySide.QtCore import Signal;
from PySide.QtGui import QLabel;

class QLabelWithDrop(QLabel):
    fileDropped = Signal(str);
    clicked = Signal();

    def __init__(self, parent):
        super(QLabelWithDrop, self).__init__(parent);

    def dragEnterEvent(self, event):
        mimeData = event.mimeData();
        if mimeData.hasUrls():
            urls = event.mimeData().urls();
            if urls[0].isLocalFile():
                event.acceptProposedAction();

    def dropEvent(self, event):
        logger.debug('dropEvent recieved');
        urls = event.mimeData().urls();
        logger.debug('URLs = %s', str(urls));
        self.fileDropped.emit(urls[0].toLocalFile());
        event.acceptProposedAction();

    def mousePressEvent(self, event):
        self.clicked.emit();
