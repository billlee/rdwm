# Copyright (c) 2014 Yang Li <bill.lee.y@gmail.com>
# All rights reserved.
import logging;
logger = logging.getLogger(__name__);

import numpy as np;
from PIL import Image;

class rwm_dummysplitter(object):
    def __init__(self, codec):
        self.codec = codec;

    def attach(self, im, text):
        logger.info('Band splitting via %s', __name__);
        mat = np.asarray(im);
        assert(len(mat.shape)==2);
        return Image.fromarray(np.array(self.codec.attach(mat, text), dtype=np.uint8));

    def detect(self, im):
        logger.info('Band splitting via %s', __name__);
        mat = np.asarray(im);
        assert(len(mat.shape)==2);
        return self.codec.detect(mat);
