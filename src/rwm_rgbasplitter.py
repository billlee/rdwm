# Copyright (c) 2014 Yang Li <bill.lee.y@gmail.com>
# All rights reserved.
import logging;
logger = logging.getLogger(__name__);

import numpy as np;
from PIL import Image;
from skimage import color;

class rwm_rgbasplitter(object):
    def __init__(self, codec):
        self.codec = codec;

    def attach(self, im, text):
        assert(im.mode=='RGB'  or im.mode=='RGBA')
        logger.info('Band splitting via %s', __name__);
        logger.debug('Transforming from RGB into LAB...');
        mat = np.asarray(im);
        mat = color.rgb2lab(self._uint2float(mat[:,:,0:3]));
        mat[:,:,0] = self.codec.attach(mat[:,:,0], text);
        logger.debug('Transforming from LAB into RGB...');
        mat = self._float2uint(color.lab2rgb(mat));
        return Image.fromarray(mat);

    def detect(self, im):
        assert(im.mode=='RGB'  or im.mode=='RGBA')
        logger.info('Band splitting via %s', __name__);
        logger.debug('Transforming from RGB into LAB...');
        mat = np.asarray(im);
        mat = color.rgb2lab(self._uint2float(mat[:,:,0:3]));
        return self.codec.detect(mat[:,:,0]);

    @staticmethod
    def _uint2float(mat):
        return mat/255.0;

    @staticmethod
    def _float2uint(mat):
        mat[mat>1.0] = 1.0;
        mat[mat<0.0] = 0.0;
        return np.asarray(255*mat, dtype=np.uint8);
