# Copyright (c) 2014 Yang Li <bill.lee.y@gmail.com>
# All rights reserved.
from distutils.core import setup
import py2exe
import numpy

def dependencies_for_myprogram():
    from scipy.sparse.csgraph import _validation;

setup(console=['rwm.py', 'derwm.py'],
        windows=['rwm-gui.py'],
        options = {
            'py2exe':{
                'compressed':True,
                'dll_excludes':['MSVCP90.dll'],
                'includes':['scipy.sparse.csgraph._validation'],
                'excludes':[
                    'matplotlib',
                    'Tkinter',
                    'PySide.QtNetwork'
                    ]
                }
            }
        );
