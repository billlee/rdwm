#!/usr/bin/env python
# Copyright (c) 2014 Yang Li <bill.lee.y@gmail.com>
# All rights reserved.

import logging;
logger = logging.getLogger(__name__);

from PIL import Image;

def main_internal(im, text):
    from rwm_fdp import rwm_fdp;
    from rwm_squarelizer import rwm_squarelizer;
    codec = rwm_squarelizer(rwm_fdp());
    if im.mode=='RGB' or im.mode=='RGBA':
        from rwm_rgbasplitter import rwm_rgbasplitter;
        codec = rwm_rgbasplitter(codec);
    elif im.mode=='L':
        from rwm_dummysplitter import rwm_dummysplitter;
        codec = rwm_dummysplitter(codec);
    else:
        raise TypeError('Image format not supported.');
    om = codec.attach(im, text);
    return om;

def main_cli():
    import buildconfig;
    import argparse;
    logging.basicConfig(level=buildconfig.log_level, format=buildconfig.log_format);
    logger.info('Robust Watermark');
    logger.debug('sys.argv: %s', sys.argv);
    logger.info('Parsing arguments...');
    parser = argparse.ArgumentParser();
    parser.add_argument('-i', '--ifile')
    parser.add_argument('-o', '--ofile');
    parser.add_argument('-l', '--log-level');
    parser.add_argument('-t', '--text', required=True, help='watermark text');
    args = parser.parse_args();
    if args.log_level:
        numberic_log_level = getattr(logging, args.log_level.upper(), None);
        if isinstance(numberic_log_level, int):
            root = logging.getLogger()
            if root.handlers:
                for handler in root.handlers:
                    root.removeHandler(handler)
            logging.basicConfig(level=numberic_log_level, format=buildconfig.log_format);
        else:
            raise ValueError('Invalid log level: %s' % args.log_level);
    logger.debug(str(args));
    if args.ifile:
        ifile = args.ifile;
    else:
        ifile = sys.stdin;
    if args.ofile:
        ofile = args.ofile;
    else:
        ofile = sys.stdout;
    logger.info('Loading image...');
    im = Image.open(ifile);
    logger.debug('Format: %s, Size: %s, Mode: %s', im.format, im.size, im.mode);
    om = main_internal(im, args.text);
    logger.info('Writing output file...');
    om.save(ofile);
    logger.debug('Format: %s, Size: %s, Mode: %s', om.format, om.size, om.mode);
    return 0;    

if __name__ == '__main__':
    import sys;
    sys.exit(main_cli());
