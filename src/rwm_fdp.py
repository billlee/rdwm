# Copyright (c) 2014 Huang Hualong, Yang Li, Pan Qianqian
# All rights reserved.
import buildconfig;
from bitarray import bitarray;
import numpy as np;
from numpy import fft;
import scipy.cluster.hierarchy as cluster;
from PIL import Image;
from PIL import ImageDraw;

import logging;
logger = logging.getLogger(__name__);

class rwm_fdp(object):
    @classmethod
    def attach(cls, mat, text):
        logger.info("Watermark attaching via %s", __name__);
        mat, wm = cls._preprocess(mat, text);
        N = buildconfig.fdp_sector_num/2;
        R = np.amax(mat.shape);
        r = np.linspace(R/8, R*3/8, buildconfig.fdp_track_num);
        centre = (mat.shape[0]/2, mat.shape[1]/2);
        theta = np.linspace(0, np.pi, N+1);
        theta = theta[:-1];
        # require outer product to generate the whole coordinate
        x = np.around(centre[0] + np.outer(r,np.cos(theta))).flatten();
        y = np.around(centre[1] + np.outer(r,np.sin(theta))).flatten();
        capacity = x.size - theta.size;
        logger.debug('capacity = %d', capacity);
        if wm.length() >= capacity:
            wm = wm[0:capacity];
            logger.warning("Watermark truncated.");
            logger.debug(str(wm));
        strength = cls._calculateAttachingAmptitude(mat,\
                cls._generateRingMask(mat.shape, r[0], r[-1]));
        wm = strength * np.array(wm.tolist(), dtype=np.bool_);
        logger.debug("Watermark data to be attach:\n%s", str(wm));
        logger.debug("Length: %d", wm.size);
        logger.debug("Attaching watermark data...");
        for i in range(theta.size, wm.size+theta.size):    # bypass the fist theta.size points
            mat[x[i],y[i]] = 1j*np.imag(mat[x[i],y[i]]) + wm[i-theta.size];
            mat[mat.shape[0]-x[i], mat.shape[1]-y[i]] = 1j*np.imag(mat[x[i],y[i]]) + wm[i-theta.size];   #TODO: maybe copying the upper half of matrix outside the loop is faster
        logger.debug("Attaching watermark header...");
        x, y = cls._polar2cartesian(r[0], theta, centre);
        header = strength * np.array([1,1,0,1]);
        # The first 2 bit will be used to detect starting point and the number of theta. The last bit will be used to detect direction
        for i in range(0, header.size):
            mat[x[i],y[i]] = 1j*np.imag(mat[x[i],y[i]]) + header[i];
            mat[mat.shape[0]-x[i], mat.shape[1]-y[i]] = 1j*np.imag(mat[x[i],y[i]]) + header[i];
        logger.debug('inverse DFTing image...');
        mat = np.absolute(np.real(fft.ifft2(mat)));
        return mat;

    @classmethod
    def detect(cls, mat):
        logger.info("Watermark detecting via %s", __name__);
        N = buildconfig.fdp_sector_num/2;
        R = np.amax(mat.shape);
        r_ideal = np.linspace(R/8, R*3/8, buildconfig.fdp_track_num);
        theta_ideal = np.linspace(0, np.pi, N+1);
        theta_ideal = theta_ideal[:-1];
        centre = (mat.shape[0]/2, mat.shape[1]/2);
        mat = np.absolute(cls._preprocess(mat));
        track_width = (r_ideal[1]-r_ideal[0])*0.4;
        logger.debug('Determining detecting threshold...');
        threshold = cls._calculateDetectingThreshold(mat,\
                cls._generateRingMask(mat.shape, r_ideal[0]-track_width, r_ideal[0]+track_width));
        logger.debug(threshold);
        logger.debug('Detecting attached points...');
        scaningMask = cls._generateRingMask(mat.shape,\
                r_ideal[0]-track_width, r_ideal[-1]+track_width);
        x,y = np.where(np.real(mat*scaningMask)>threshold);
        r,theta = cls._cartesian2polar(x,y,centre);
        logger.debug('Determing tracks...');
        r_class = cluster.fclusterdata(r[:,np.newaxis], (r_ideal[1]-r_ideal[0])/2, criterion='distance') - 1;   # minutes one to shift the minimum class number become zero
        r_class_centre = np.zeros(max(r_class)+1);
        for i in range(0, r_class_centre.size):
            r_class_centre[i] = np.average(r[r_class==i]);
        r_index = np.argsort(r_class_centre);   # r_index[i] is the index of the i-th little element
        r_rank = np.argsort(r_index);   # r_rank[i] is the rank of element[i]
        r_class = r_rank[r_class];
        logger.debug(r_class);
        logger.debug('Determing sectors...');
        theta_class = cluster.fclusterdata(theta[:,np.newaxis], (theta_ideal[1]-theta_ideal[0])/2, criterion='distance') - 1;
        theta_class_centre = np.zeros(max(theta_class)+1);
        for i in range(0, theta_class_centre.size):
            theta_class_centre[i] = np.average(theta[theta_class==i]);
        theta_class_centre_sorted = np.sort(theta_class_centre);
        theta_diff = np.amin(np.diff(theta_class_centre_sorted));
        theta_rank = np.array(np.around((theta_class_centre-theta_class_centre_sorted[0])/theta_diff), dtype=np.uint8);
        theta_class = theta_rank[theta_class];
        logger.debug(theta_class);
        logger.debug('Spinning sectors...');
        header, = np.where(r_class==0);
        if header.size!=6:
            logger.error('Corrupt header: %s', str(header));
            raise ValueError('Detection of watermark failed with corrupt header.');
        header_theta_class_sorted = np.sort(theta_class[header]);
        header_theta_class_diff = np.diff(header_theta_class_sorted);
        theta_class_base_index, = np.where(header_theta_class_diff==1);
        theta_class_base = header_theta_class_sorted[theta_class_base_index[0]];
        theta_class = (theta_class-theta_class_base)%(N*2);
        logger.debug(theta_class);
        logger.debug('Recovering watermark...');
        theta_class = theta_class[r_class>0];
        r_class = r_class[r_class>0]-1;
        r_class = r_class[theta_class<N];
        theta_class = theta_class[theta_class<N];
        wm = np.zeros(N*(r_ideal.size), dtype=np.bool_);
        wm[r_class*N+theta_class] = True;
        wm = bitarray(wm.tolist());
        return wm.tobytes();

    @staticmethod
    def _calculateAttachingAmptitude(mat, scaningMask):
        data = np.absolute(np.real(mat[scaningMask]));
        return data.max()*3;

    @staticmethod
    def _calculateDetectingThreshold(mat, scaningMask):
        data = np.real(mat[scaningMask]);
        # determine threshold via binary search
        hi = data.max();
        lo = 0;
        while True:
            tmp = (lo+hi)/2;
            count = data[data>tmp].size;
            if count==6:
                return tmp;
            elif count>6:
                lo = tmp;
            else:
                hi = tmp;

    @staticmethod
    def _preprocess(mat, text=None):
        logger.debug('DFTing image...');
        mat = fft.fftshift(fft.fft2(mat));
        if text is None:
            return mat;
        logger.debug('Transforming watermark text into bitarray...');
        wm = bitarray();
        wm.frombytes(text);
        return mat, wm;

    @staticmethod
    def _generateRingMask(shape, r_min, r_max):
        # Use mode 'L' as a workaround of buggy mode '1'
        black = 0;
        white = 255;
        im = Image.new('L', shape);
        draw = ImageDraw.Draw(im);
        centre = (shape[0]/2, shape[1]/2);
        xy = ((centre[0]-r_max, centre[1]-r_max),\
                (centre[0]+r_max, centre[1]+r_max));
        draw.ellipse(xy, fill=white, outline=white);
        xy = ((0,0),\
                (centre[0],shape[1]));
        xy = ((centre[0]-r_min+1, centre[1]-r_min+1),\
                (centre[0]+r_min-1, centre[1]+r_min-1));
        draw.ellipse(xy, fill=black, outline=black);
        return np.array(np.array(im), dtype=np.bool_);

    @staticmethod
    def _polar2cartesian(r, theta, centre):
        x = np.around(centre[0] + r*np.cos(theta));
        y = np.around(centre[1] + r*np.sin(theta));
        return x,y;

    @staticmethod
    def _cartesian2polar(x, y, centre):
        x = x-centre[0];
        y = y-centre[0];
        r = np.hypot(x, y);
        theta = np.arctan2(y,x);
        return r,theta;
