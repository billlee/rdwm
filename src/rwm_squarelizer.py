# Copyright (c) 2014 Yang Li <bill.lee.y@gmail.com>
# All rights reserved.
import logging;
logger = logging.getLogger(__name__);
from scipy.ndimage.interpolation import zoom;

class rwm_squarelizer(object):
    def __init__(self, codec):
        self.codec = codec;

    def attach(self, mat, text):
        logger.info('Squarelizing via %s', __name__);
        shape = mat.shape;
        length = float(max(shape));
        mat = zoom(mat, (length/shape[0],length/shape[1]));
        mat = self.codec.attach(mat, text);
        return zoom(mat, (shape[0]/length,shape[1]/length));

    def detect(self, mat):
        logger.info('Squarelizing via %s', __name__);
        shape = mat.shape;
        length = float(max(shape));
        mat = zoom(mat, (length/shape[0],length/shape[1]));
        return self.codec.detect(mat);
