%% 程序说明 该程序包括自适应设计以及参数估计
%% 测试部分 自适应处理颜色；
%考虑以下干扰：噪声：椒盐噪声+高斯噪声；几何变换：缩放，旋转；裁剪
% 待解决问题：
% 1、怎么估计最大水印信息量，测试结果，理想容量 (R_max^2-R_min^2)*pi 实际容量 
% 1.5、怎么提高水印嵌入信息的大小？建议解决方案：1、先找到最佳角度上限（凭借测试），再找到最佳半径数上限（凭借量化标准，PSNR）
% 2、嵌入强度的决定方案：1、始终由最大值确定；2、由平均值确定
% 3、嵌入水印点数的确定，利用特定模式，该模式兼有开始标志位和旋转标志位的作用
% 4、水印量化评价 PSNR
% 6、为避免tan角度检测对于0的失败，角度添加一个偏移分量（但是要克服偏移角度刚好是的tan为0的情况），
% 8、准备一个开始标志位，一个正确提取标志位，水印提取办法：1、利用较少的约定标志位计算出门限值；2、直接估计门限值，取消自适应；目前采用第一种。
% 9、（x，y）->（R，theta），注意(0,0)是很特殊的点，要特别处理
% 10、水印扫描过程这里应该只是扫描一半即可
% 11、嵌入角度的个数与嵌入起始标志位之间存在对应关系，或者使用固定数目的嵌入角度
% 12、修改起始标志的角度值，并且每一个圈的第一个数值约定为1
%% 读取图片，缩放预处理 频域变换
clear all
%读取图片，图片备选列表：lenna baboon pepper
lena=imread('baboon.bmp');lena=lena(:,:,1);%取第一色道
len=length(lena);%获取图片边长
figure(1),imshow(lena);%显示原始图像 
dlena=fftshift(fft2(double(lena)));%原始图像的频域图像
%figure(2);imshow(log(1+dlena)*0.035);%显示频域图像

%% 在频域上加标记点 注：保证对称性可以降低对于峰值检验时候的误差
%嵌入区域为同心圆，构造嵌入半径集合R，嵌入角度集合theta
R_num=4;theta_num=16/2;%此处R_num>=2
x_center=len/2;y_center=len/2;%同心圆圆心

R_max=len*3/8;R_min=len/8;%R取值区间[R_min R_max]
R=(0:R_num)*(R_max-R_min)/(R_num+1)+R_min;%R取值线性排列

theta=(0.5:theta_num-1+0.5)*pi/theta_num;%theta取值区间[0 pi]，取值线性排列
capacity=R_num*theta_num;%实际水印容量
capacity_ideal=(R_max^2-R_min^2)*pi;%最大水印容量估计
%watermark=ones(1,R_num*theta_num);

watermark=[1,1,1,1,1,0,1,1, 1,0,1,0,1,0,1,1];%水印信息
%判断水印程度是否超过容量
% if length(watermark)>capacity
%   disp('overflow!'）    
% end   

%确定嵌入强度
%% 此处是否可以只获取半径最小的一圈的数值最大值
%% 
%计算中频区域最大强度
eps_R=3;%允许R的误差范围
max_strength=0;
    %扫描半个圆
    for x=1:len/2
        for y=1:len
            if ( (min(R)-eps_R)<sqrt((x-x_center)^2+( y-y_center )^2) ) & ( sqrt((x-x_center)^2+( y-y_center )^2)<(max(R)+eps_R) )
                if dlena(x,y)>max_strength
                    max_strength=dlena(x,y);
                end
            end
        end
    end
%% 参数
embed_coeff=3;
mark_strength=embed_coeff*max_strength;%嵌入标记幅度==中频区域最大幅度*3

%嵌入算法；注：嵌入标记点对称
temp=dlena;
for i=1:length(watermark)
    %选取嵌入半径，嵌入角度
    R_selected=R(ceil(i/theta_num)+1);theta_selected=theta(ceil(mod(i-1,theta_num))+1);%预留第一圈作为标志位区，theta取遍1：theta_num    
    x=ceil(x_center+R_selected*cos(theta_selected));
    y=ceil(y_center+R_selected*sin(theta_selected));%取整
    %中心对称嵌入标记点,peak_embed记录嵌入结果
    if watermark(i)==1
        temp(x,y)=imag(temp(x,y))*1i+mark_strength;
        temp(len+1-x,len+1-y)=imag(temp(len+1-x,len+1-y))*1i+mark_strength;
        peak_embed(i,1)=x;peak_embed(i,2)=y;
        peak_embed(i+length(watermark),1)=len+1-x;peak_embed(i+length(watermark),2)=len+1-y;
    end
end

% 起始标志位+旋转标志位 合二为一 特别嵌入一圈 用以估计提取门限值 旋转角度
start=[1 1 1 1];
theta_start=(0.5:theta_num-1+0.5)*pi/theta_num;
%% 固定嵌入4*2个点
%%
for i=1:length(start)
    R_selected=R(1);theta_selected=theta_start(ceil(mod(i-1,theta_num))+1);    
    x=ceil(x_center+R_selected*cos(theta_selected));
    y=ceil(y_center+R_selected*sin(theta_selected));
    if start(i)==1
        temp(x,y)=imag(temp(x,y))*1i+mark_strength;
        temp(len+1-x,len+1-y)=imag(temp(len+1-x,len+1-y))*1i+mark_strength;
        peak_start(i,1)=x;peak_start(i,2)=y;
        peak_start(i+length(start),1)=len+1-x;peak_start(i+length(start),2)=len+1-y;
    end
end

theta_no_rotate=mean(theta_start(:));%嵌入位置（0，len/10）
% 
%     temp(x_rotate,y_rotate)=imag(temp(x_rotate,y_rotate))+2*mark_strength;
%     temp(len+1-x_rotate,len+1-y_rotate)=imag(temp(len+1-x_rotate,len+1-y_rotate))*1i+mark_strength;

%% 嵌入后图像
image=uint8(ifft2(fftshift(temp)));
figure(2);imshow(image);
lena_embed=image;
%% 剪切
% resize_coef=0.75;
% figure(7),imshow(imcrop(lena,[1 1 resize_coef*len resize_coef*len]));
% x_center=x_center*resize_coef;y_center=y_center*resize_coef;len=len*resize_coef;R=len/4;

%% 旋转 注意：旋转会引起图片尺寸的变换
% imag=imrotate(imag,30);
% figure(7),imshow(imag);
% resize_coef=length(imag)/len;
% x_center=x_center*resize_coef;y_center=y_center*resize_coef;len=len*resize_coef;R=len/4;

%% 缩放 考虑缩放系数的影响
% resize_coef=0.75;
% imag=imresize(imag,resize_coef);
% figure(7),imshow(imag);
% x_center=x_center*resize_coef;y_center=y_center*resize_coef;len=len*resize_coef;R=len/4;

%% 添加椒盐噪声
%  image=imnoise(image,'salt & pepper',.7);
%  figure(7),imshow(image);

%% 添加高斯噪声
% imag=imnoise(imag,'gaussian',0.035);
% figure(7),imshow(imag);
%% 三维显示
% pic=temp;  
% [x y]=size(pic);
% [x y]=meshgrid(1:x,1:y);
% figure(7);plot3(x,y,pic);

%% 自适应检测嵌入点 先乱序整体检测符合条件的点，再重排分类
temp=fftshift(fft2(double(lena_embed)));

%确定门限强度
eps_R=3;%允许R误差范围
%确定中频区域最大系数，用以估计提取门限值
max_strength=0;
    %扫描半个圆
    for x=1:len/2
        for y=1:len
            if ( (min(R)-eps_R)<sqrt((x-x_center)^2+( y-y_center )^2) ) & ( sqrt((x-x_center)^2+( y-y_center )^2)<(max(R)+eps_R) )
                if temp(x,y)>max_strength
                    max_strength=temp(x,y);
                end
            end
        end
    end
%% 参数
abstract_coeff=0.5;
thold=abstract_coeff*max_strength;%以中频区域最大系数的1/2作为起点,这里可能直接使用最大值就比较稳妥了
% 确定门限值
%% 参数
step=1000;%此处阀值有所改变
eps_num=0;%检测点波动范围
eps_R=3;%检测半径波动范围
%检测起始标志位，确定门限值
len_start=8;
while true   
    i=0;flag=1;
    %此处可以只扫描半个圆
    for x=1:len
        for y=1:len
            if ( (min(R)-eps_R)<sqrt((x-x_center)^2+( y-y_center )^2) ) & ( sqrt((x-x_center)^2+( y-y_center )^2)<(min(R)+eps_R) ) & ( real(temp(x,y))>=thold )
                i=i+1;                 
                if i>2*len_start
                    flag=0;break;
                end
            end
        end
        if flag==0
            break;
        end
    end
    if abs(i-len_start)==eps_num
        i
        thold
        break;
    else
        thold=thold+sign(i-len_start)*step;
        i
        thold
    end
end
%% 参数 thold定值检测
thold=thold*1;
eps_num=2;%检测点数量波动范围
eps_R=3;%检测半径波动范围
i=0;
  for x=1:len
        for y=1:len
            if ( (min(R)-eps_R)<sqrt((x-x_center)^2+( y-y_center )^2) ) & ( sqrt((x-x_center)^2+( y-y_center )^2)<(max(R)+eps_R) ) & ( real(temp(x,y))>=thold )
                i=i+1; 
                peak_check(i,1)=x;peak_check(i,2)=y;%记录峰值检测结果
            end
        end
  end
 
%% 解码 (x,y)->(R,theta) 不能解决非常接近中点的提取问题
% 由于考虑了整个圆，存在冗余
R_check=sqrt( (peak_check(:,1)-x_center).^2+(peak_check(:,2)-y_center).^2 );

theta_check=( (peak_check(:,1)-x_center)>0 & (peak_check(:,2)-y_center)>0 ).*atan((peak_check(:,2)-y_center)./(peak_check(:,1)-x_center))+...
            ( (peak_check(:,1)-x_center)<0 & (peak_check(:,2)-y_center)>0 ).*(pi+atan((peak_check(:,2)-y_center)./(peak_check(:,1)-x_center)))+...
            ( (peak_check(:,1)-x_center)<0 & (peak_check(:,2)-y_center)<0 ).*(pi+atan((peak_check(:,2)-y_center)./(peak_check(:,1)-x_center)))+...
            ( (peak_check(:,1)-x_center)>0 & (peak_check(:,2)-y_center)<0 ).*(2*pi+atan((peak_check(:,2)-y_center)./(peak_check(:,1)-x_center)));

% 检测出的水印嵌入信息 [ R theta x y ]
watermark_check=[R_check theta_check  peak_check(:,1) peak_check(:,2)];

%分组 对R和theta分别进行排序 
[R_sorted index]=sort(watermark_check(:,1));
%约定水印嵌入范围[ len/8 len*3/8]
%% 参数 单层问题没有解决
R_group_num=find(diff(R_sorted)>((R_max-R_min)/R_num/2));%此处门限值的选择比较困难
if length(R_group_num)>0
    watermark_check(1:index(R_group_num(1)),5)=1;
    for i=1:length(R_group_num)-1
        watermark_check(index(R_group_num(i)+1:R_group_num(i+1)),5)=i+1;
    end
    watermark_check(index(R_group_num(i+1)+1:length(index)),5)=i+2;
end

%% 单层问题没有解决
[theta_sorted index]=sort(watermark_check(:,2));
theta_group_num=find(diff(theta_sorted)>((1/4)*pi/(theta_num)));
%假设角度嵌入范围就是[0 pi]
if theta_group_num>=0
    watermark_check(1:index(theta_group_num(1)),6)=1;
    for i=1:length(theta_group_num)-1
        watermark_check(index(theta_group_num(i)+1:theta_group_num(i+1)),6)=i+1;
    end
    watermark_check(index(theta_group_num(i+1)+1:length(index)),6)=i+2;
end 

%%
% 重排结构
[data index]=sort(watermark_check(:,5)*2*(theta_num+1)+watermark_check(:,6));
result=watermark_check(index,:);

% 扩充重排结构
theta_diff=mean(diff(result(1:4,2)));
R_diff=R_sorted(R_group_num(1)+1)-R_sorted(R_group_num(1));

theta_rotate=mean(result(1:4,2));
theta_rotate_diff=theta_rotate-theta_no_rotate;

result(1:8,:)=[];

water=zeros(2*length(watermark),1);
%此处应该划分区间
water((round( (result(:,1)-min(R))/R_diff)-1)*(2*round((pi/theta_diff))+1)+round((result(:,2)-0.5*pi/theta_num)/theta_diff)+1)=1;
water'
%{
%% 旋转角度检测

%% 水印品质量化标准 PSNR
D=double(lena)-double(lena_embed);
MSE=sum(D(:).^2)/len^2;
PSNR=10*log10((2^len-1)^2/MSE)
%}