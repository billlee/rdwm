@echo off
cd src
pyside-uic rwm-gui.ui -o ui_rwm_gui.py
pyside-uic about.ui -o ui_about.py
if "%1"=="exe" (
    setup.py py2exe --dist-dir ..\dist
    echo D | xcopy /F C:\Python27\Lib\site-packages\PySide\plugins\imageformats ..\dist\imageformats
    copy ..\NOTICE.binary ..\dist\NOTICE.txt
    copy ..\doc\MANUAL ..\dist\MANUAL.txt
)
cd ..
@echo on
